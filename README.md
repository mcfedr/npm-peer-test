# Testing optional peer deps

Here you see

## `pack`

this is a package, `@mcfedr/pack`, with optional peer dependency on react

its published to gitlab registry at https://gitlab.com/mcfedr/npm-peer-test/-/packages/15500674

## `pack-npm`

this is a package, `test-gitlab-peer-pack-npm`, with optional peer dependency on lodash

its published to npm at https://www.npmjs.com/package/test-gitlab-peer-pack-npm

## `proj`

this is a project, with deps on the above two, and you can see that the optional peer dep of react is installed
but that lodash is not.

## apis

from gitlab, `https://gitlab.com/api/v4/projects/47084852/packages/npm/@mcfedr/pack`

no peer deps meta in the response

from npm, `https://registry.npmjs.org/test-gitlab-peer-pack-npm`

peer deps meta in the response
